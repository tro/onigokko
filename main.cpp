#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#pragma comment(lib, "winmm.lib")

#define FRAME_MILLISEC 30
#define _CRT_SECURE_NO_WARNINGS 1

// プロトタイプ宣言　2015/02/22 原
LRESULT CALLBACK wndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);
void _OutputDebugString_(LPCSTR pszFormat, ...);

// エントリーポイントwinmain 2015/02/22 原
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASS winc;
	winc.style         = CS_HREDRAW | CS_VREDRAW;
	winc.lpfnWndProc   = wndProc;
	winc.cbClsExtra    = winc.cbWndExtra = 0;
	winc.hInstance     = hInstance;
	winc.hIcon         = LoadIcon(NULL,IDI_APPLICATION);
	winc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	winc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	winc.lpszMenuName  = NULL;
	winc.lpszClassName = TEXT("Block");
	if (!RegisterClass(&winc)) return 0; 

	HWND hwnd = CreateWindow(
		TEXT("Block"),
		TEXT("Block"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 
		CW_USEDEFAULT,
		480,
		320,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (!hwnd) return 0;
	ShowWindow(hwnd,SW_SHOW);

	/*
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) 
	{ 
		DispatchMessage(&msg); 
	}
	*/
	/*
	MSG msg;
	while (TRUE) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) 
		{
			if (!GetMessage(&msg, NULL, 0, 0)) break;
			TranslateMessage(&msg);
		    DispatchMessage(&msg);
		}
	}
	*/
	
	MSG msg;
	DWORD startTime = 0;
	DWORD endTime = 0;
	DWORD sleepTime = 0;
	DWORD lastFlameTime = 0;
	int frameCount = 0;
	while (TRUE) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) 
		{
			if (!GetMessage(&msg, NULL, 0, 0)) break;
			TranslateMessage(&msg);
		    DispatchMessage(&msg);
		}
		else 
		{
			// TODO:描画処理

			// 画面更新
			InvalidateRect(hwnd, NULL, FALSE);

			endTime = timeGetTime();
			DWORD sleep = FRAME_MILLISEC - (endTime - startTime);

			if (sleep <= 0 || sleep > FRAME_MILLISEC) {
			    _OutputDebugString_("%ld %ld, %d\n",endTime - startTime, sleep, frameCount);
				sleep = 1;
			}
			Sleep(sleep);
			startTime = timeGetTime();

			// FPS デバッグ用
			++frameCount;
			sleepTime += sleep;
			if (startTime - lastFlameTime >= 1000) 
			{
				_OutputDebugString_("%d CPU:%ld %%\n", frameCount, (startTime - lastFlameTime - sleepTime) * 100 / (startTime - lastFlameTime));
				lastFlameTime = startTime;
				frameCount = sleepTime = 0;
			}


		}
	}

	return msg.wParam;
}

// イベントハンドラ 2015/02/22 原
LRESULT CALLBACK wndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
    switch(msg)
	{
	case WM_DESTROY:
		return 0;
	}
	return DefWindowProc(hwnd, msg, wp,lp);
}

// デバッガ関数 2015/02/22 原
void _OutputDebugString_(LPCSTR pszFormat, ...)
{
    va_list arep;
	char pszBuf[1024];
	va_start(arep, pszFormat);
	vsprintf(pszBuf, pszFormat, arep);
	va_end(arep);
	OutputDebugStringA(pszBuf);
}







